%% @author USER
%% @doc @todo Add description to part1.


-module(part1).
-compile(export_all).

helper(X) when X rem 2 =:= 0 -> X;
helper(X) -> X+1.

distributecandy(_,_,_,T) when T =:= 0 -> io:fwrite("Turn cant be zero");
distributecandy(S1,S2,S3,_) when ((S1-S2 =:= 0) and (S1-S3 =:= 0)) -> io:fwrite("Initial candy distribution cant be zero or goal is reached");
distributecandy(S1,S2,S3,T) -> 
	P = round(S1/2),
	Q = round(S2/2),
	R = round(S3/2),
	M = helper(P+R),
	N = helper(Q+P),
	O = helper(R+Q),
	io:fwrite("s1=~w, s2=~w, s3=~w T=~w ~n", [M,N,O,T]),
	distributecandy(M,N,O,T-1). 
