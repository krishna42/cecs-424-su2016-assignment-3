-module(str1).
-compile(export_all).

readlines(Filename) ->
    {ok, Device} = file:open(Filename, read),
	string:tokens(io:get_line(Device, "")," .,<>=()\"").

key_find(Key, List) ->
    case lists:keyfind(Key, 1, List) of
    	{Key, Result} -> 
			lists:append([lists:delete({Key, Result}, List), [{Key, Result+1}]]);
		false -> lists:append([List,[{Key,1}]])
	end.

hash_check(Filename) ->
	Words = (str1:readlines(Filename)),
	lists:foldl(fun(X,Tl) -> str1:key_find(string:to_lower(X), Tl) end, [],Words).

final_hash(Filename) ->
	lists:sort(fun({KeyA,ValA}, {KeyB,ValB}) -> {ValA,KeyA} >= {ValB,KeyB} end,str1:hash_check(Filename)).